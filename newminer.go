package main

import "net/http"
import "fmt"
import "time"
import "encoding/json"
import "encoding/hex"
import "strings"
import "sync"

type WaitForBlock struct {
	Hash   string `json:"hash"`
	Height uint64 `json:"height"`
}

type Chaintip struct {
	Height uint64 `json:"height"`
	Hash   string `json:"hash"`
	Status string `json:"status"`
}

type state struct {
	our_block_at_height   map[uint64]string
	our_top_block_hash    string
	our_top_block_height  uint64
	last_known_btc_height uint64

	TxMutex sync.RWMutex
	Tx      map[[32]byte]map[uint32][2]uint64

	ExtMutex sync.RWMutex
	ExtTopBlockHash string
	ExtTopBlockHeight uint64
}

func (s *state) Get(tx [32]byte, out uint32) (height uint64, value uint64) {
	s.TxMutex.RLock()
	defer s.TxMutex.RUnlock()

	two := s.Tx[tx][out]

	//println(out, fmt.Sprintf("%x", tx), two[0], two[1])

	height, value = two[0], two[1]
	return
}

func (s *state) get_chain_size() uint64 {
	return s.our_top_block_height
}
func (s *state) GetTopBlockAndHeight() (string, uint64) {
	s.ExtMutex.RLock()
	defer s.ExtMutex.RUnlock()

	return s.ExtTopBlockHash, s.ExtTopBlockHeight
}

func (s *state) handle_reorg_direct(up, down uint64) {

	fmt.Println("handle_reorg_direct:", up, down)

	hash := s.our_block_at_height[down]
	s.our_top_block_hash = hash
	s.our_top_block_height = down
}

func (s *state) mine_blocks(client *http.Client, down, up uint64) {

	fmt.Println("mine_blocks:", down, up)

	for height := down; height <= up; height++ {

		block, err := dlBlock(client, height)
		if err != nil {
			return
		}
		if block.Time == 0 {
			println("Error: No time in block")
			println("please upgrade to the latest version of combdownloader")
			println("then, enable usecase pricecharts.")
			return
		}
		s.TxMutex.Lock()
		if block.Tx != nil {
			for _, tx := range block.Tx {
				var txid, err = hex.DecodeString(tx.Txid)
				if err != nil {
					continue
				}
				var txidbuf [32]byte
				copy(txidbuf[:], txid)
				//var txidbuforig = txidbuf
				for i := 0; i < 16; i++ {
					txidbuf[i], txidbuf[31-i] = txidbuf[31-i], txidbuf[i]
				}
				s.Tx[txidbuf] = make(map[uint32][2]uint64)
				//s.Tx[txidbuforig] = make(map[uint32][2]uint64)
				for i, out := range tx.Out {
					//println(height, fmt.Sprintf("%x", txidbuf), i, uint64(out.Value * 100000000))
					s.Tx[txidbuf][uint32(i)] = [2]uint64{height, uint64(out.Value * 100000000)}
					//s.Tx[txidbuforig][uint32(i)] = [2]uint64{height, uint64(out.Value * 100000000)}
				}
			}
		}
		s.TxMutex.Unlock()
		s.our_block_at_height[height] = block.Hash
		s.our_top_block_hash = block.Hash
		s.our_top_block_height = height

		if height == up {
			s.ExtMutex.Lock()
			s.ExtTopBlockHash = s.our_top_block_hash
			s.ExtTopBlockHeight = s.our_top_block_height
			s.ExtMutex.Unlock()
		}
	}

}

func (s *state) find_reorg2(client *http.Client, hc_height uint64) (uint64, error) {

	fmt.Println("find_reorg: started")
	var lowest uint64 = 1

	for hc_height > lowest {

		var ours = s.our_block_at_height[hc_height]

		result_json, err := make_bitcoin_call(client, "getblockhash", fmt.Sprint(hc_height))
		if err != nil {
			return 0, err
		}
		result := strings.Trim(string(result_json), `"`)

		if result == ours {
			return hc_height, nil
		}

		hc_height--
	}
	return lowest, nil
}

func new_miner_start(new_miner_height uint64, minerInterface *minerInterface) {
	// make the http client for main calls
	var http_client = make_client(true)
	var s state
	*minerInterface = &s
	for {
		result_json, err := make_bitcoin_call(http_client, "getblockhash", fmt.Sprint(new_miner_height))
		if err != nil {
			fmt.Println("get_block_info: block hash call error")
			continue
		}

		var new_miner_hash = strings.Trim(string(result_json), `"`)

		s.TxMutex.Lock()
		s.Tx = make(map[[32]byte]map[uint32][2]uint64)
		s.our_block_at_height = make(map[uint64]string)
		s.our_top_block_hash = new_miner_hash
		s.our_top_block_height = new_miner_height
		s.last_known_btc_height = 0
		s.our_block_at_height[new_miner_height] = new_miner_hash

		s.TxMutex.Unlock()
		break
	}

outer:
	for {
		time.Sleep(time.Second)

		chainTipData, err2 := make_bitcoin_call(http_client, "getchaintips", "")
		if err2 != nil {
			continue
		}

		var chainTips []Chaintip
		err3 := json.Unmarshal(chainTipData, &chainTips)
		if err3 != nil {
			continue
		}

		var activeHash string
		var activeHeight, headersOnlyHeight uint64
		for _, tip := range chainTips {
			switch tip.Status {
			case "active":
				activeHeight = tip.Height
				activeHash = tip.Hash
				if headersOnlyHeight < tip.Height {
					headersOnlyHeight = tip.Height
				}
			case "headers-only", "valid-headers":
				if headersOnlyHeight < tip.Height {
					headersOnlyHeight = tip.Height
				}
			}
		}

		if activeHeight != headersOnlyHeight {
			//fmt.Println("activeHeight:", activeHeight, "headersOnlyHeight:", headersOnlyHeight)
			continue
		}

		hash := activeHash
		height := uint64(activeHeight)

		// We are synced, wait for block
		if s.our_top_block_hash == activeHash {
			if dualWaiters(func(i, j int) error {
				data, err1 := make_bitcoin_call(http_client, "waitfornewblock", fmt.Sprintf("%d", i))
				if err1 != nil {
					fmt.Println(err1.Error())
					return err1
				}
				var wfb WaitForBlock
				err4 := json.Unmarshal(data, &wfb)
				if err4 != nil {
					fmt.Println(err4.Error())
					return err4
				}

				if wfb.Hash != activeHash {
					hash = wfb.Hash
					height = wfb.Height
					return fmt.Errorf("new_block")
				}
				return nil
			}, 4).Error() != "new_block" {
				continue
			}
		}

		s.last_known_btc_height = uint64(height)

		// Get Haircomb's highest known block
		comb_height := uint64(s.get_chain_size())

		var ourhash_at_btc_height = s.our_block_at_height[height]

		fmt.Println(hash, "==", ourhash_at_btc_height)

		if ourhash_at_btc_height == hash {
			fmt.Println("case 1")
			fmt.Println("reorg type \"direct\"")
			s.handle_reorg_direct(uint64(comb_height), uint64(height))
			continue outer
		}

		fmt.Println(height, "<=", comb_height)

		if height <= comb_height {
			target_height, err := s.find_reorg2(http_client, height)
			if err != nil {
				fmt.Println("mine_loop: find reorg err:", err)
				continue outer
			}
			fmt.Println("case 2")
			fmt.Println("reorg type \"direct\"")
			s.handle_reorg_direct(uint64(comb_height), uint64(target_height))
			continue outer
		}

		result_json, err := make_bitcoin_call(http_client, "getblockhash", fmt.Sprint(comb_height))
		if err != nil {
			fmt.Println("get_block_info: block hash call error")
			continue outer
		}

		var btc_hash_at_our_height = strings.Trim(string(result_json), `"`)

		var our_highest_block = s.our_top_block_hash

		fmt.Println("?", btc_hash_at_our_height, our_highest_block)

		if btc_hash_at_our_height == our_highest_block {

		} else {
			var target_height uint64

			target_height, err = s.find_reorg2(http_client, comb_height)
			if err != nil {
				fmt.Println("mine_loop: find reorg err:", err)
				continue outer
			}
			fmt.Println("case 3")
			fmt.Println("reorg type \"direct\"")
			s.handle_reorg_direct(uint64(comb_height), uint64(target_height))
			continue outer
		}
		fmt.Println("case 4")
		// finally fast forward
		s.mine_blocks(http_client, uint64(comb_height)+1, uint64(height))
		continue outer
	}

}
