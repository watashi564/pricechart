package main

type minerInterface interface {
	Get(tx [32]byte, out uint32) (height uint64, value uint64)
	GetTopBlockAndHeight() (string, uint64)
}

type minerInterfaceImpl struct {
	m minerInterface
}

func (m *minerInterfaceImpl) Get(tx [32]byte, out uint32) (height uint64, value uint64) {
	if m == nil || m.m == nil {
		return 0, 0
	}
	return m.m.Get(tx, out)
}

func (m *minerInterfaceImpl) GetTopBlockAndHeight() (string, uint64) {
	if m == nil || m.m == nil {
		return "", 0
	}
	return m.m.GetTopBlockAndHeight()
}
