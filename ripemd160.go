package main

import "encoding/binary"

func f1(x, y, z uint32) uint32 {
	return x ^ y ^ z
}
func f2(x, y, z uint32) uint32 {
	return (x & y) | (^x & z)
}
func f3(x, y, z uint32) uint32 {
	return (x | ^y) ^ z
}
func f4(x, y, z uint32) uint32 {
	return (x & z) | (y & ^z)
}
func f5(x, y, z uint32) uint32 {
	return x ^ (y | ^z)
}

type RipeMD160 struct {
	bytes  uint64
	buffer [64]byte
	s      [5]uint32
	buf    []byte //= buffer[0:]
}

/** Initialize RIPEMD-160 state. */
func (r *RipeMD160) Initialize() {
	r.s[0] = 0x67452301
	r.s[1] = 0xEFCDAB89
	r.s[2] = 0x98BADCFE
	r.s[3] = 0x10325476
	r.s[4] = 0xC3D2E1F0
	r.buf = r.buffer[0:]
}

func rol(x uint32, i int) uint32 {
	return (x << i) | (x >> (32 - i))
}

func Round(a *uint32, b uint32, c *uint32, d uint32, e uint32, f uint32,
	x uint32, k uint32, r int) {
	*a = rol(*a+f+x+k, r) + e
	*c = rol(*c, 10)
}

func R11(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f1(b, *c, d), x, 0, r)
}
func R21(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f2(b, *c, d), x, 0x5A827999, r)
}
func R31(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f3(b, *c, d), x, 0x6ED9EBA1, r)
}
func R41(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f4(b, *c, d), x, 0x8F1BBCDC, r)
}
func R51(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f5(b, *c, d), x, 0xA953FD4E, r)
}

func R12(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f5(b, *c, d), x, 0x50A28BE6, r)
}
func R22(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f4(b, *c, d), x, 0x5C4DD124, r)
}
func R32(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f3(b, *c, d), x, 0x6D703EF3, r)
}
func R42(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f2(b, *c, d), x, 0x7A6D76E9, r)
}
func R52(a *uint32, b uint32, c *uint32, d uint32, e uint32, x uint32, r int) {
	Round(a, b, c, d, e, f1(b, *c, d), x, 0, r)
}

/** Perform a RIPEMD-160 transformation, processing a 64-byte chunk. */
func transform(s *[5]uint32, chunk []byte) {
	var a1, b1, c1, d1, e1 = s[0], s[1], s[2], s[3], s[4]
	var a2, b2, c2, d2, e2 = a1, b1, c1, d1, e1
	var w0 = binary.LittleEndian.Uint32(chunk[0:4])
	var w1 = binary.LittleEndian.Uint32(chunk[4:8])
	var w2 = binary.LittleEndian.Uint32(chunk[8:12])
	var w3 = binary.LittleEndian.Uint32(chunk[12:16])
	var w4 = binary.LittleEndian.Uint32(chunk[16:20])
	var w5 = binary.LittleEndian.Uint32(chunk[20:24])
	var w6 = binary.LittleEndian.Uint32(chunk[24:28])
	var w7 = binary.LittleEndian.Uint32(chunk[28:32])
	var w8 = binary.LittleEndian.Uint32(chunk[32:36])
	var w9 = binary.LittleEndian.Uint32(chunk[36:40])
	var w10 = binary.LittleEndian.Uint32(chunk[40:44])
	var w11 = binary.LittleEndian.Uint32(chunk[44:48])
	var w12 = binary.LittleEndian.Uint32(chunk[48:52])
	var w13 = binary.LittleEndian.Uint32(chunk[52:56])
	var w14 = binary.LittleEndian.Uint32(chunk[56:60])
	var w15 = binary.LittleEndian.Uint32(chunk[60:64])

	R11(&a1, b1, &c1, d1, e1, w0, 11)
	R12(&a2, b2, &c2, d2, e2, w5, 8)
	R11(&e1, a1, &b1, c1, d1, w1, 14)
	R12(&e2, a2, &b2, c2, d2, w14, 9)
	R11(&d1, e1, &a1, b1, c1, w2, 15)
	R12(&d2, e2, &a2, b2, c2, w7, 9)
	R11(&c1, d1, &e1, a1, b1, w3, 12)
	R12(&c2, d2, &e2, a2, b2, w0, 11)
	R11(&b1, c1, &d1, e1, a1, w4, 5)
	R12(&b2, c2, &d2, e2, a2, w9, 13)
	R11(&a1, b1, &c1, d1, e1, w5, 8)
	R12(&a2, b2, &c2, d2, e2, w2, 15)
	R11(&e1, a1, &b1, c1, d1, w6, 7)
	R12(&e2, a2, &b2, c2, d2, w11, 15)
	R11(&d1, e1, &a1, b1, c1, w7, 9)
	R12(&d2, e2, &a2, b2, c2, w4, 5)
	R11(&c1, d1, &e1, a1, b1, w8, 11)
	R12(&c2, d2, &e2, a2, b2, w13, 7)
	R11(&b1, c1, &d1, e1, a1, w9, 13)
	R12(&b2, c2, &d2, e2, a2, w6, 7)
	R11(&a1, b1, &c1, d1, e1, w10, 14)
	R12(&a2, b2, &c2, d2, e2, w15, 8)
	R11(&e1, a1, &b1, c1, d1, w11, 15)
	R12(&e2, a2, &b2, c2, d2, w8, 11)
	R11(&d1, e1, &a1, b1, c1, w12, 6)
	R12(&d2, e2, &a2, b2, c2, w1, 14)
	R11(&c1, d1, &e1, a1, b1, w13, 7)
	R12(&c2, d2, &e2, a2, b2, w10, 14)
	R11(&b1, c1, &d1, e1, a1, w14, 9)
	R12(&b2, c2, &d2, e2, a2, w3, 12)
	R11(&a1, b1, &c1, d1, e1, w15, 8)
	R12(&a2, b2, &c2, d2, e2, w12, 6)

	R21(&e1, a1, &b1, c1, d1, w7, 7)
	R22(&e2, a2, &b2, c2, d2, w6, 9)
	R21(&d1, e1, &a1, b1, c1, w4, 6)
	R22(&d2, e2, &a2, b2, c2, w11, 13)
	R21(&c1, d1, &e1, a1, b1, w13, 8)
	R22(&c2, d2, &e2, a2, b2, w3, 15)
	R21(&b1, c1, &d1, e1, a1, w1, 13)
	R22(&b2, c2, &d2, e2, a2, w7, 7)
	R21(&a1, b1, &c1, d1, e1, w10, 11)
	R22(&a2, b2, &c2, d2, e2, w0, 12)
	R21(&e1, a1, &b1, c1, d1, w6, 9)
	R22(&e2, a2, &b2, c2, d2, w13, 8)
	R21(&d1, e1, &a1, b1, c1, w15, 7)
	R22(&d2, e2, &a2, b2, c2, w5, 9)
	R21(&c1, d1, &e1, a1, b1, w3, 15)
	R22(&c2, d2, &e2, a2, b2, w10, 11)
	R21(&b1, c1, &d1, e1, a1, w12, 7)
	R22(&b2, c2, &d2, e2, a2, w14, 7)
	R21(&a1, b1, &c1, d1, e1, w0, 12)
	R22(&a2, b2, &c2, d2, e2, w15, 7)
	R21(&e1, a1, &b1, c1, d1, w9, 15)
	R22(&e2, a2, &b2, c2, d2, w8, 12)
	R21(&d1, e1, &a1, b1, c1, w5, 9)
	R22(&d2, e2, &a2, b2, c2, w12, 7)
	R21(&c1, d1, &e1, a1, b1, w2, 11)
	R22(&c2, d2, &e2, a2, b2, w4, 6)
	R21(&b1, c1, &d1, e1, a1, w14, 7)
	R22(&b2, c2, &d2, e2, a2, w9, 15)
	R21(&a1, b1, &c1, d1, e1, w11, 13)
	R22(&a2, b2, &c2, d2, e2, w1, 13)
	R21(&e1, a1, &b1, c1, d1, w8, 12)
	R22(&e2, a2, &b2, c2, d2, w2, 11)

	R31(&d1, e1, &a1, b1, c1, w3, 11)
	R32(&d2, e2, &a2, b2, c2, w15, 9)
	R31(&c1, d1, &e1, a1, b1, w10, 13)
	R32(&c2, d2, &e2, a2, b2, w5, 7)
	R31(&b1, c1, &d1, e1, a1, w14, 6)
	R32(&b2, c2, &d2, e2, a2, w1, 15)
	R31(&a1, b1, &c1, d1, e1, w4, 7)
	R32(&a2, b2, &c2, d2, e2, w3, 11)
	R31(&e1, a1, &b1, c1, d1, w9, 14)
	R32(&e2, a2, &b2, c2, d2, w7, 8)
	R31(&d1, e1, &a1, b1, c1, w15, 9)
	R32(&d2, e2, &a2, b2, c2, w14, 6)
	R31(&c1, d1, &e1, a1, b1, w8, 13)
	R32(&c2, d2, &e2, a2, b2, w6, 6)
	R31(&b1, c1, &d1, e1, a1, w1, 15)
	R32(&b2, c2, &d2, e2, a2, w9, 14)
	R31(&a1, b1, &c1, d1, e1, w2, 14)
	R32(&a2, b2, &c2, d2, e2, w11, 12)
	R31(&e1, a1, &b1, c1, d1, w7, 8)
	R32(&e2, a2, &b2, c2, d2, w8, 13)
	R31(&d1, e1, &a1, b1, c1, w0, 13)
	R32(&d2, e2, &a2, b2, c2, w12, 5)
	R31(&c1, d1, &e1, a1, b1, w6, 6)
	R32(&c2, d2, &e2, a2, b2, w2, 14)
	R31(&b1, c1, &d1, e1, a1, w13, 5)
	R32(&b2, c2, &d2, e2, a2, w10, 13)
	R31(&a1, b1, &c1, d1, e1, w11, 12)
	R32(&a2, b2, &c2, d2, e2, w0, 13)
	R31(&e1, a1, &b1, c1, d1, w5, 7)
	R32(&e2, a2, &b2, c2, d2, w4, 7)
	R31(&d1, e1, &a1, b1, c1, w12, 5)
	R32(&d2, e2, &a2, b2, c2, w13, 5)

	R41(&c1, d1, &e1, a1, b1, w1, 11)
	R42(&c2, d2, &e2, a2, b2, w8, 15)
	R41(&b1, c1, &d1, e1, a1, w9, 12)
	R42(&b2, c2, &d2, e2, a2, w6, 5)
	R41(&a1, b1, &c1, d1, e1, w11, 14)
	R42(&a2, b2, &c2, d2, e2, w4, 8)
	R41(&e1, a1, &b1, c1, d1, w10, 15)
	R42(&e2, a2, &b2, c2, d2, w1, 11)
	R41(&d1, e1, &a1, b1, c1, w0, 14)
	R42(&d2, e2, &a2, b2, c2, w3, 14)
	R41(&c1, d1, &e1, a1, b1, w8, 15)
	R42(&c2, d2, &e2, a2, b2, w11, 14)
	R41(&b1, c1, &d1, e1, a1, w12, 9)
	R42(&b2, c2, &d2, e2, a2, w15, 6)
	R41(&a1, b1, &c1, d1, e1, w4, 8)
	R42(&a2, b2, &c2, d2, e2, w0, 14)
	R41(&e1, a1, &b1, c1, d1, w13, 9)
	R42(&e2, a2, &b2, c2, d2, w5, 6)
	R41(&d1, e1, &a1, b1, c1, w3, 14)
	R42(&d2, e2, &a2, b2, c2, w12, 9)
	R41(&c1, d1, &e1, a1, b1, w7, 5)
	R42(&c2, d2, &e2, a2, b2, w2, 12)
	R41(&b1, c1, &d1, e1, a1, w15, 6)
	R42(&b2, c2, &d2, e2, a2, w13, 9)
	R41(&a1, b1, &c1, d1, e1, w14, 8)
	R42(&a2, b2, &c2, d2, e2, w9, 12)
	R41(&e1, a1, &b1, c1, d1, w5, 6)
	R42(&e2, a2, &b2, c2, d2, w7, 5)
	R41(&d1, e1, &a1, b1, c1, w6, 5)
	R42(&d2, e2, &a2, b2, c2, w10, 15)
	R41(&c1, d1, &e1, a1, b1, w2, 12)
	R42(&c2, d2, &e2, a2, b2, w14, 8)

	R51(&b1, c1, &d1, e1, a1, w4, 9)
	R52(&b2, c2, &d2, e2, a2, w12, 8)
	R51(&a1, b1, &c1, d1, e1, w0, 15)
	R52(&a2, b2, &c2, d2, e2, w15, 5)
	R51(&e1, a1, &b1, c1, d1, w5, 5)
	R52(&e2, a2, &b2, c2, d2, w10, 12)
	R51(&d1, e1, &a1, b1, c1, w9, 11)
	R52(&d2, e2, &a2, b2, c2, w4, 9)
	R51(&c1, d1, &e1, a1, b1, w7, 6)
	R52(&c2, d2, &e2, a2, b2, w1, 12)
	R51(&b1, c1, &d1, e1, a1, w12, 8)
	R52(&b2, c2, &d2, e2, a2, w5, 5)
	R51(&a1, b1, &c1, d1, e1, w2, 13)
	R52(&a2, b2, &c2, d2, e2, w8, 14)
	R51(&e1, a1, &b1, c1, d1, w10, 12)
	R52(&e2, a2, &b2, c2, d2, w7, 6)
	R51(&d1, e1, &a1, b1, c1, w14, 5)
	R52(&d2, e2, &a2, b2, c2, w6, 8)
	R51(&c1, d1, &e1, a1, b1, w1, 12)
	R52(&c2, d2, &e2, a2, b2, w2, 13)
	R51(&b1, c1, &d1, e1, a1, w3, 13)
	R52(&b2, c2, &d2, e2, a2, w13, 6)
	R51(&a1, b1, &c1, d1, e1, w8, 14)
	R52(&a2, b2, &c2, d2, e2, w14, 5)
	R51(&e1, a1, &b1, c1, d1, w11, 11)
	R52(&e2, a2, &b2, c2, d2, w0, 15)
	R51(&d1, e1, &a1, b1, c1, w6, 8)
	R52(&d2, e2, &a2, b2, c2, w3, 13)
	R51(&c1, d1, &e1, a1, b1, w15, 5)
	R52(&c2, d2, &e2, a2, b2, w9, 11)
	R51(&b1, c1, &d1, e1, a1, w13, 6)
	R52(&b2, c2, &d2, e2, a2, w11, 11)

	var t uint32 = s[0]
	s[0] = s[1] + c1 + d2
	s[1] = s[2] + d1 + e2
	s[2] = s[3] + e1 + a2
	s[3] = s[4] + a1 + b2
	s[4] = t + b1 + c2
}

func (r *RipeMD160) Write(data []byte) {

	var bufsize = r.bytes % 64
	if bufsize != 0 && bufsize+uint64(len(data)) >= 64 {
		// Fill the buffer, and process it.
		copy(r.buf[bufsize:64], data[0:64-bufsize])
		r.bytes += 64 - bufsize
		data = data[64-bufsize:]
		transform(&r.s, r.buf)
		bufsize = 0
	}
	for len(data) >= 64 {
		// Process full chunks directly from the source.
		transform(&r.s, data)
		r.bytes += 64
		data = data[64:]
	}
	if len(data) > 0 {
		// Fill the buffer with what remains.
		copy(r.buf[bufsize:64], data[0:])
		r.bytes += uint64(len(data))
	}
}

func (r *RipeMD160) Finalize() (hash [20]byte) {
	var pad = [64]byte{0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0}
	var sizedesc [8]byte
	binary.LittleEndian.PutUint64(sizedesc[:], r.bytes<<3)
	r.Write(pad[0 : 1+((119-(r.bytes%64))%64)])
	r.Write(sizedesc[:])
	binary.LittleEndian.PutUint32(hash[:], r.s[0])
	binary.LittleEndian.PutUint32(hash[4:], r.s[1])
	binary.LittleEndian.PutUint32(hash[8:], r.s[2])
	binary.LittleEndian.PutUint32(hash[12:], r.s[3])
	binary.LittleEndian.PutUint32(hash[16:], r.s[4])
	return hash
}

func (r *RipeMD160) Reset() {
	r.bytes = 0
	r.Initialize()
}