package main

import "bufio"
import "os"
import "io"
import "strings"
import "strconv"
import "fmt"
import "math"

//const chartstart int = 0//95000
const clustering = 1
const zeroes = false

const timeclustering = true

//const timemodulo int64 = 60*60*7*24

func coins(n int64) float64 {
	var log2 = math.Log2(float64(n))
	var decrease = log2 * log2 * log2 * log2 * log2 * log2
	return 2.10000000 - math.Floor(decrease/100000000.)
}

func price(chartstart int, timemodulo int64) {
	var height []int64
	var timestamp []int64
	var price []int64
	file, err := os.Open("price.txt")
	if err != nil {
		panic(err)
	}
	reader := bufio.NewReaderSize(file, 1024*1024)

	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			panic(err)
		}

		str := strings.Split(string(line), "\t")

		for i := 0; i < len(str); i++ {
			v, _ := strconv.Atoi(str[i])
			if i == 0 {
				height = append(height, int64(v))
			}
			if i == 1 {
				timestamp = append(timestamp, int64(v))
			}
			if i == 2 {
				if v < 0 {
					v = 0
				}
				price = append(price, int64(v))
			}
		}
	}

	f, err := os.Create("out.js")
	if err != nil {
		println(err.Error())
	}

	w := bufio.NewWriter(f)

	fmt.Fprint(w, `var rawdata = [`)

	var cluster = clustering

	if timeclustering {
		cluster = 1
	}

	for i := chartstart; i+cluster+1 < len(timestamp); i += cluster {

		if timeclustering {
			cluster = 1
			for ; i+cluster+1 < len(timestamp); cluster++ {

				var left = timestamp[i]
				var right = timestamp[i+cluster]

				if left/timemodulo != right/timemodulo {
					break
				}

			}
		}

		var min = float64(price[i]) / coins(height[i])
		var max = float64(price[i]) / coins(height[i])
		for j := i; j < i+cluster; j++ {
			var pri = float64(price[j]) / coins(height[j])
			if zeroes || pri != 0 {
				if pri < min {
					min = pri
				}
				if pri > max {
					max = pri
				}
			}
		}

		var left = timestamp[i]
		var right = timestamp[i+cluster-1]

		var avg = (left + right) / 2

		var price_left = float64(price[i]) / coins(height[i])
		var price_right = float64(price[i+cluster-1]) / coins(height[i+cluster-1])

		if !zeroes {
			for j := 0; j <= cluster; j++ {
				var pri = float64(price[i+j]) / coins(height[i+j])
				if pri != 0 {
					price_left = pri
					break
				}
			}

			for j := 0; j <= cluster; j++ {
				var pri = float64(price[i+cluster-j]) / coins(height[i+cluster-j])
				if pri != 0 {
					price_right = pri
					break
				}
			}
		}
		_ = price_left
		_ = price_right

		fmt.Fprintf(w, `{
              x: new Date(%d),
              y: [%.0f, %.0f, %.0f, %.0f]
            },`, avg*1000, price_left, max, min, price_right)

	}

	fmt.Fprint(w, `];`)
	w.Flush()
	f.Close()
}