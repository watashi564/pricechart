package main

import "github.com/syndtr/goleveldb/leveldb"
import "flag"
import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/net/http2"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strings"
	"time"
)
import (
	"bufio"
	"os"
)
import (
	"encoding/binary"
	"encoding/hex"
)
import "sync"

type SPubKey struct {
	Type string `json:"type"`
	Hex  string `json:"hex"`
}
type Input struct {
	Vout uint32 `json:"vout"`
	TxId string `json:"txid"`
}
type Output struct {
	ScriptPubKey *SPubKey `json:"scriptPubKey,omitempty"`
	Value        float64  `json:"value"`
}
type Transaction struct {
	Txid string   `json:"txid"`
	Out  []Output `json:"vout"`
	In   []Input  `json:"vin"`
}
type Block struct {
	Hash   string        `json:"hash"`
	Height uint64        `json:"height"`
	Tx     []Transaction `json:"tx"`
	Time   uint64        `json:"time"`
}

type RPC_Result struct {
	Result json.RawMessage `json:"result"`
}

type File struct {
	Success bool        `json:"Success"`
	Chart   [][2]uint64 `json:"Chart"`
}

func dial_no_tls[T any](ctx context.Context, network, addr string, _ *T) (net.Conn, error) {
	var d net.Dialer
	return d.DialContext(ctx, network, addr)
}

func make_client(use_http2 bool) *http.Client {
	if use_http2 {

		tr := &http2.Transport{
			AllowHTTP: true,
		}
		tr.DialTLSContext = dial_no_tls
		client := &http.Client{
			Transport: tr,
		}
		return client
	}
	client := &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: 130,
		},
		Timeout: 24 * time.Hour,
	}
	return client
}
func make_bitcoin_call(client *http.Client, method, params string) (json.RawMessage, error) {

	const ip = "127.0.0.1"
	port := "8332"
	body := strings.NewReader("{\"jsonrpc\":\"1.0\",\"id\":\"curltext\",\"method\":\"" + method + "\",\"params\":[" + params + "]}")
	req, err := http.NewRequest("POST", "http://:@"+ip+":"+port, body)

	if err != nil {
		log.Fatal("phone btc ERROR", err)
	}
	req.Header.Set("Content-Type", "text/plain")

	resp, err := client.Do(req)
	if err != nil {
		//set_connected(false)
		return nil, errors.New("no_btc" + err.Error())
	}

	defer resp.Body.Close()
	resp_bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.New("body_read_error_3")
	}

	// Check for a return value
	if len(resp_bytes) == 0 {
		fmt.Println("BTC Comms Error: Likely an incorrect RPC username or password. Please make sure the username and password in Haircomb's config.txt file match the ones stored in Bitcoin's bitcoin.conf file, then restart both programs.")
		return nil, errors.New("bad_log_info")
	}

	var result RPC_Result

	err = json.Unmarshal(resp_bytes, &result)
	if err != nil {
		return nil, errors.New("bad_json")
	}

	//set_connected(true)

	return result.Result, nil

}
func dlMaxHeight(client *http.Client) (n uint64, err error) {
	height_json, err := make_bitcoin_call(client, "getblockcount", "null")
	if err != nil {
		return 0, err
	}
	h := string(height_json)
	for i := range h {
		n *= 10
		n += uint64(h[i]) - uint64('0')
	}

	return n, nil
}

func dlBlock(client *http.Client, height uint64) (*Block, error) {

	result_json, err := make_bitcoin_call(client, "getblockhash", fmt.Sprint(height))
	if err != nil {
		return nil, err
	}
	if result_json == nil {
		return nil, errors.New("bad_json")
	}
	block_json, err := make_bitcoin_call(client, "getblock", fmt.Sprintf("%s", result_json)+", "+"3")
	if err != nil {
		return nil, err
	}
	if block_json == nil {
		return nil, errors.New("bad_json")
	}
	if fmt.Sprintf("%v", block_json) == "[]" {
		return nil, errors.New("bad_json" + fmt.Sprintf("%v", block_json))
	}

	var result *Block
	err = json.Unmarshal(block_json, &result)
	if err != nil {
		return nil, errors.New("bad_json" + fmt.Sprintf("%v", block_json))
	}
	if result == nil {
		return nil, errors.New("no_result")
	}
	if result.Height != height {
		fmt.Println(result.Height, height)
		return nil, errors.New("bad_height")
	}
	if `"`+result.Hash+`"` != fmt.Sprintf("%s", result_json) {
		fmt.Println(result.Hash, fmt.Sprintf("%s", result_json))
		return nil, errors.New("bad_hash")
	}
	return result, nil
}
func encodeUint(x uint64) []byte {
	buf := make([]byte, 8)
	binary.BigEndian.PutUint64(buf, x)
	return buf
}
func decodeUint(x []byte) uint64 {
	return binary.BigEndian.Uint64(x)
}
func hashTxAndHeight(tx string, height uint64) [20]byte {
	ripemd := RipeMD160{}
	ripemd.Initialize()
	ripemd.Write([]byte(tx))
	ripemd.Write(encodeUint(height))
	hash := ripemd.Finalize()
	return hash
}
func realheight(h uint64, b1, b2 byte) uint64 {
	return ((((h >> 16) << 8) | uint64(b1)) << 8) | uint64(b2)
}
func reversehex(buf string) string {
	obuf := make([]byte, len(buf))
	for i := 0; 2*i < len(buf); i += 2 {
		obuf[i], obuf[len(buf)-i-2] = buf[len(buf)-i-2], buf[i]
		obuf[i+1], obuf[len(buf)-i-1] = buf[len(buf)-i-1], buf[i+1]
	}
	return string(obuf)
}

const valueBufferConstant = 24

func main() {
	var chartstart, timemodulo, waithours int
	var earlystart bool
	flag.IntVar(&chartstart, "chartstart", 0, "Start at n-th block, 0 is the haircomb genesis roughly.")
	flag.IntVar(&timemodulo, "timemodulo", 60*60*24*7, "One candle per n seconds. Weekly candles by default (60*60*24*7).")
	flag.IntVar(&waithours, "waithours", 0, "Wait number of hours at the end.")
	flag.BoolVar(&earlystart, "earlystart", false, "Open the webserver early.")
	flag.Parse()

	b, _ := leveldb.OpenFile("db/", nil)

	var height uint64 = 1
	heightBytes, err := b.Get([]byte{0}, nil)

	if err == nil && len(heightBytes) == 8 {

		height = decodeUint(heightBytes)
		println("loaded height", height)
		height++
	}

	client := make_client(true)

	maxHeight, err := dlMaxHeight(client)
	if err != nil || maxHeight == 0 {
		println("loaded max height is wrong:", maxHeight)
		return
	}

	if earlystart {

		var miner minerInterfaceImpl
		go new_miner_start(maxHeight-6, &miner.m)
		go Serve(&dbInterfaceImpl{b}, &miner)

	}

	batch := new(leveldb.Batch)
	thisblock := make(map[[20]byte][3]byte)

	var progress Bar
	progress.Unit = "blocks"
	progress.Graph = "#"

	var mut sync.Mutex
	prev_block, prev_err := dlBlock(client, height)
	prev_height := height
	block, err := prev_block, prev_err

	for height+6 <= maxHeight {

		mut.Lock()
		if height == prev_height {
			block, err = prev_block, prev_err
		} else {
			mut.Unlock()
			time.Sleep(time.Millisecond)
			continue
		}
		mut.Unlock()

		go func(height uint64) {
			mut.Lock()
			prev_block, prev_err = dlBlock(client, height)
			prev_height = height
			mut.Unlock()
		}(height + 1)

		for err != nil || block == nil {
			time.Sleep(time.Millisecond)
			if err != nil {
				println(err.Error())
			}
			block, err = dlBlock(client, height)
		}

		if block.Time == 0 {
			println("Error: No time in block")
			println("please upgrade to the latest version of combdownloader")
			println("then, enable usecase pricecharts.")
			return
		}

		if block.Tx != nil {

			var claimed bool
		outer:
			for _, tx := range block.Tx {
				for _, out := range tx.Out {
					if out.ScriptPubKey != nil {
						h, err := hex.DecodeString(out.ScriptPubKey.Hex)
						if err != nil {
							println(err.Error())
							break
						}
						if (len(h) != 34) || (h[0] != 0) || (h[1] != 32) {
							continue
						}
						h = h[2:]

						_, err = b.Get(h[:], nil)
						if err == leveldb.ErrNotFound {
							if height <= 601000 || claimed {
								batch.Put(h[:], encodeUint(height))
								continue
							}
						} else if err != nil {
							println(err.Error())
							break
						} else {
							continue
						}
						claimed = true
						//println("Found claim", out.ScriptPubKey.Type, out.ScriptPubKey.Hex)

						buffer := encodeUint(height)

						var sum uint64
						for _, out := range tx.Out {
							sum += uint64(out.Value * 100000000)
						}

						buffer = append(buffer, encodeUint(sum)...)
						buffer = append(buffer, encodeUint(block.Time)...)

						for _, in := range tx.In {

							for prevheight := height; prevheight <= height; prevheight -= 65536 {

								//if prevheight == 0 {
								//	println(reversehex(in.TxId))
								//}

								prevhash := hashTxAndHeight(in.TxId, prevheight>>16)
								prevvalue, err := b.Get(prevhash[:], nil)
								if err == leveldb.ErrNotFound {
									prevval, ok := thisblock[prevhash]

									if !ok {
										continue
									} else {
										prevvalue = prevval[:]
										err = nil
									}
								}
								if err != nil {
									println(err.Error())
									continue
								}

								if claimed {
									// make tx that went to claim undeleteable
									if prevvalue[0] < 255 {
										prevvalue[0] = 255
									}
									thisblock[prevhash] = [3]byte{prevvalue[0], prevvalue[1], prevvalue[2]}
								}

								prevheight = realheight(prevheight, prevvalue[1], prevvalue[2])

								buffer = append(buffer, []byte(in.TxId)...)
								buffer = append(buffer, encodeUint(prevheight)...)
								buffer = append(buffer, encodeUint(uint64(in.Vout))...) //577608
								//println(" - ", reversehex(in.TxId), prevheight)
								break
							}
						}

						batch.Put(h[:], buffer)

						break outer
					}

				}

			}
			for _, tx := range block.Tx {

				hash := hashTxAndHeight(tx.Txid, height>>16)

				var outcount [3]byte
				if len(tx.Out) >= 255 {
					outcount = [3]byte{255, byte(height >> 8), byte(height)}
				} else {
					outcount = [3]byte{byte(len(tx.Out)), byte(height >> 8), byte(height)}
				}

				if outcount[0] > 0 {

					thisblock[hash] = outcount
				}

				//fmt.Printf("%x", hash)

				for _, in := range tx.In {
					if in.TxId == "0000000000000000000000000000000000000000000000000000000000000000" {
						continue
					}

					for prevheight := height; prevheight <= height; prevheight -= 65536 {
						//if prevheight == 0 {
						//	println(reversehex(in.TxId))
						//}
						prevhash := hashTxAndHeight(in.TxId, prevheight>>16)

						prevvalue, err := b.Get(prevhash[:], nil)

						if err == leveldb.ErrNotFound {

							prevval, ok := thisblock[prevhash]

							if !ok {
								continue
							} else {
								prevvalue = prevval[:]
								err = nil
							}
						}
						if err != nil {
							println(err.Error())
							break
						}
						if (len(prevvalue) == 0) || (prevvalue[0] == 255) {
							break
						}

						//prevheight = realheight(prevheight, prevvalue[1], prevvalue[2])

						if prevvalue[0] <= 1 {

							thisblock[prevhash] = [3]byte{0, 0, 0}

							//println("GC", reversehex(in.TxId), prevheight)
						} else {
							prevvalue[0]--

							thisblock[prevhash] = [3]byte{prevvalue[0], prevvalue[1], prevvalue[2]}

						}
						break
					}

				}
			}

		}
		batch.Put([]byte{0}, encodeUint(height))

		if ((height & 7) == 0) || (height+6 == maxHeight) {
			for k, v := range thisblock {
				if v[0] == 0 {
					batch.Delete(k[:])
				} else {

					batch.Put(k[:], v[:])
				}
			}

			err = b.Write(batch, nil)
			if err != nil {
				println(err.Error())
				break
			}
			batch = new(leveldb.Batch)
			thisblock = make(map[[20]byte][3]byte)
		}

		progress.Play(0, int64(height), int64(maxHeight))

		height++
	}
	progress.Hide()
	println("Entering pass 2... Fees evaluation for all claims.")

	var heightaccumulator = make(map[uint64][]string)
	var voutaccumulator = make(map[string][]uint64)

	iter := b.NewIterator(nil, nil)
	for iter.Next() {

		key := iter.Key()
		value := iter.Value()

		if (len(key) != 32) || (len(value) <= 16) || (len(value) == 32) {
			// this is not a claim
			continue
		}

		var height = decodeUint(value[0:8])
		//var sum = decodeUint(value[8:16])

		if height <= 601000 {
			// we are not interested about fees for pre-comb-genesis claims
			continue
		}

		for i := valueBufferConstant; i < len(value); i += 80 {

			var prevheight = decodeUint(value[i+64 : i+64+8])
			var vout = decodeUint(value[i+64+8 : i+64+8+8])

			var tx = string(value[i : i+64])

			h := hashTxAndHeight("utxo"+tx, vout)

			_, err = b.Get(h[:], nil)
			if err == leveldb.ErrNotFound {

				heightaccumulator[prevheight] = append(heightaccumulator[prevheight], tx)
				voutaccumulator[tx] = append(voutaccumulator[tx], vout)
				//fmt.Printf("ACCUMULATED %s %d \n", reversehex(tx), vout)
				//fmt.Printf("%d %d %x %s %d %d \n", height, sum, key, value[i:i+64], prevheight, vout)

			}
		}

	}
	iter.Release()
	err = iter.Error()
	if err != nil {
		println(err.Error())
	}

	progress.Restart("blocks")

	var cnt int64
	for height, txs := range heightaccumulator {

		progress.Play(0, int64(cnt), int64(len(heightaccumulator)))
		cnt++

		block, err := dlBlock(client, height)
		for err != nil || block == nil {
			if err != nil {
				println(err.Error())
			}
			time.Sleep(time.Millisecond)
			block, err = dlBlock(client, height)
			continue
		}

		for _, tx := range txs {
			if block.Tx != nil {
				for _, tx2 := range block.Tx {
					if tx2.Txid != tx {
						continue
					}

					for _, v := range voutaccumulator[tx] {
						h := hashTxAndHeight("utxo"+tx, v)

						batch.Put(h[:], encodeUint(uint64(tx2.Out[v].Value*100000000)))

						//fmt.Printf("PUTTED %s %d \n", reversehex(tx), v)
					}

					break
				}
			}
		}

		err = b.Write(batch, nil)
		if err != nil {
			println(err.Error())
			break
		}
		batch = new(leveldb.Batch)
	}

	heightaccumulator = nil
	voutaccumulator = nil

	iter = b.NewIterator(nil, nil)
outer2:
	for iter.Next() {

		key := iter.Key()
		value := iter.Value()

		if (len(key) != 32) || (len(value) <= 16) || (len(value) == 32) {
			// this is not a claim
			continue
		}

		var height = decodeUint(value[0:8])
		var sum = decodeUint(value[8:16])

		if height <= 601000 {
			// we are not interested about fees for pre-comb-genesis claims
			continue
		}

		var totalamount uint64

		for i := valueBufferConstant; i < len(value); i += 80 {

			var prevheight = decodeUint(value[i+64 : i+64+8])
			var vout = decodeUint(value[i+64+8 : i+64+8+8])

			var tx = string(value[i : i+64])

			h := hashTxAndHeight("utxo"+tx, vout)

			data, err := b.Get(h[:], nil)
			if err == leveldb.ErrNotFound {
				fmt.Printf("ERROR GET %d %d %x %s %d %d \n", height, sum, key, reversehex(tx), prevheight, vout)
				continue outer2
			}
			if err != nil {
				println(err.Error())
				continue outer2
			}
			var amount = decodeUint(data)

			totalamount += amount

			batch.Delete(h[:])
		}

		batch.Put(key, append(value[0:valueBufferConstant], encodeUint(totalamount)...))

	}

	iter.Release()
	err = iter.Error()
	if err != nil {
		println(err.Error())
	}

	progress.Hide()
	println("final compacting...")
	err = b.Write(batch, nil)
	if err != nil {
		println(err.Error())
	}
	println("final compacted...")

	finalmap := make([][2]uint64, maxHeight, maxHeight)
	//finalutxo := make([]string, maxHeight, maxHeight)

	iter = b.NewIterator(nil, nil)
	for iter.Next() {

		key := iter.Key()
		value := iter.Value()

		if (len(key) != 32) || (len(value) != valueBufferConstant+8) {
			// this is not a claim
			continue
		}

		var height = decodeUint(value[0:8])
		var sum = decodeUint(value[8:16])
		var time = decodeUint(value[16:24])
		var amt = decodeUint(value[24:32])

		if height <= 601000 {
			// we are not interested about fees for pre-comb-genesis claims
			continue
		}
		finalmap[height][0] = time // put here blk time
		if amt > sum {
			finalmap[height][1] = amt - sum
		}
		//finalutxo[height] = string(key)
	}

	iter.Release()
	err = iter.Error()
	if err != nil {
		println(err.Error())
	}

	f, err := os.Create("price.txt")
	if err != nil {
		println(err.Error())
	}

	w := bufio.NewWriter(f)
	var jsondata File
	jsondata.Success = true

	for h := uint64(0); h < maxHeight; h++ {
		if finalmap[h][0] != 0 {
			jsondata.Chart = append(jsondata.Chart, [2]uint64{h, finalmap[h][1]})
			fmt.Fprintf(w, "%d\t%d\t%d\n", h, finalmap[h][0], finalmap[h][1])
		}
	}

	w.Flush()
	f.Close()
	filejsondata, err := json.Marshal(jsondata)
	if err != nil {
		println(err.Error())
	}
	err = ioutil.WriteFile("chartdisk.json", filejsondata, 0644)
	if err != nil {
		println(err.Error())
	}
	price(chartstart, int64(timemodulo))

	if !earlystart {

		var miner minerInterfaceImpl
		go new_miner_start(maxHeight-6, &miner.m)
		go Serve(&dbInterfaceImpl{b}, &miner)

	}


	time.Sleep(time.Duration(waithours) * time.Hour)

	b.Close()

	os.Exit(5)
}
