package main

import "golang.org/x/net/http2"
import "golang.org/x/net/http2/h2c"
import "io"
import "encoding/json"
import "encoding/hex"
import "fmt"
import "net"
import "net/http"
import "time"
import "sync"
import "os"
import "strings"

type TxId [32]byte

func (b *TxId) UnmarshalJSON(data []byte) error {
	var hexString string

	if err := json.Unmarshal(data, &hexString); err != nil {
		return err
	}

	decoded, err := hex.DecodeString(hexString)
	if err != nil {
		return err
	}
	copy(b[:], decoded)
	return nil

}

// MarshalJSON serializes ByteArray to hex
func (s TxId) MarshalJSON() ([]byte, error) {
	bytes, err := json.Marshal(fmt.Sprintf("%x", s))
	return bytes, err
}

type txOutPoint struct {
	TxId  TxId
	Index uint32
}
type txIn struct {
	PreviousOutPoint txOutPoint
	SignatureScript  []byte
	Amount           int64
}
type txOut struct {
	Index      uint32
	Commitment TxId
	Amount     uint64
}
type Request struct {
	TxId           TxId
	WitnessTxId    TxId
	TxIn           []txIn
	TxOut          []txOut
	OutAmountTotal int64
	Weight         int64
	Size           int64
}
type Response struct {
	TxId        TxId
	WitnessTxId TxId
	TxOut       []txOut
	Weight      int64
	Size        int64
	Fee         int64
	FeeSizeKb   int64
	FeeWeightKb int64
}

var localMutex sync.RWMutex
var local = make(map[[32]byte]map[uint32]uint64)

func (req *Request) dispatch(s *server) (r *Response) {

	localMutex.Lock()
	local[req.TxId] = make(map[uint32]uint64)
	localMutex.Unlock()
	for _, out := range req.TxOut {
		localMutex.Lock()
		local[req.TxId][out.Index] = out.Amount
		localMutex.Unlock()
		//println(fmt.Sprintf("%x", req.TxId), j, out.Amount)

		var point txOutPoint
		point.TxId = req.TxId
		point.Index = out.Index
		if h := s.db.Exists(point, 1000000); h > 0 {
			fmt.Printf("exists: %x\n", point.TxId)
			return nil
		}
	}
	var isResolving = false
	for _, in := range req.TxIn {
		if in.Amount == 0 {
			localMutex.RLock()
			localValue, localOk := local[in.PreviousOutPoint.TxId][in.PreviousOutPoint.Index]
			localMutex.RUnlock()
			if localOk {
				in.Amount = int64(localValue)
				continue
			}

			height, value := s.m.Get(in.PreviousOutPoint.TxId, in.PreviousOutPoint.Index)
			if height == 0 && value == 0 {
				if h := s.db.Exists(in.PreviousOutPoint, 1000000); h == 0 {
					//fmt.Printf("[A] not exists: %x\n", in.PreviousOutPoint.TxId)

					return nil
				} else {
					in.Amount = s.db.Amount(in.PreviousOutPoint)
					if in.Amount == 0 {
						s.GoResolve(h, in.PreviousOutPoint.TxId, req)
						isResolving = true
						//fmt.Printf("exists at height: %x %d\n", in.PreviousOutPoint.TxId, h)
					}
				}
			} else {
				in.Amount = int64(value)
			}
		}

	}
	if isResolving {
		return nil
	}
	return req.dispatch2(s)
}
func (req *Request) dispatch2(s *server) (r *Response) {
	var amountTotalIn int64
	for _, in := range req.TxIn {
		if in.Amount == 0 {
			localMutex.RLock()
			localValue, localOk := local[in.PreviousOutPoint.TxId][in.PreviousOutPoint.Index]
			localMutex.RUnlock()
			if localOk {
				in.Amount = int64(localValue)
			} else {

				height, value := s.m.Get(in.PreviousOutPoint.TxId, in.PreviousOutPoint.Index)
				if height == 0 && value == 0 {
					//fmt.Printf("[A] amount nil: %x %d - %x\n",
					//	in.PreviousOutPoint.TxId, in.PreviousOutPoint.Index, req.TxId)
					return nil
				} else {
					in.Amount = int64(value)
				}
			}
		}
		amountTotalIn += in.Amount
	}
	fee := amountTotalIn - req.OutAmountTotal

	fmt.Printf("%x %x %d %d %d\n", req.TxId, req.WitnessTxId, fee, 1000*fee/req.Weight, 1000*fee/req.Size)
	for _, out := range req.TxOut {
		fmt.Printf("%x\n", out.Commitment)
	}
	return &Response{
		TxId:        req.TxId,
		WitnessTxId: req.WitnessTxId,
		TxOut:       req.TxOut,
		Weight:      req.Weight,
		Size:        req.Size,
		Fee:         fee,
		FeeSizeKb:   1000 * fee / req.Size,
		FeeWeightKb: 1000 * fee / req.Weight,
	}
}
func (s *server) process(r *Response, data []byte) {
	s.mutex2.Lock()
	s.done[r.TxId] = string(data)
	s.mutex2.Unlock()
	time.Sleep(2 * time.Second)
	s.mutex2.Lock()
	delete(s.done, r.TxId)
	s.mutex2.Unlock()
}

func controller_send_disk_file(w http.ResponseWriter, name, ctype string) {
	data, _ := os.ReadFile(name)
	w.Header().Set("Content-Type", ctype)
	_, _ = w.Write([]byte("p("))
	_, _ = w.Write(data)
	_, _ = w.Write([]byte(");"))
}

func (s *server) mainPage(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		s.mutex3.Lock()
		nobody := s.listeners == 0
		s.mutex3.Unlock()

		if nobody {
			println("no listeners")
			return
		}

		defer r.Body.Close()

		reqBytes, err := io.ReadAll(r.Body)
		if err != nil {
			println("cannot read all")
			return
		}
		//fmt.Printf("%s\n", reqBytes)
		var req Request

		err = json.Unmarshal(reqBytes, &req)
		if err != nil {
			println("cannot unmarshal")
			return
		}
		resp := req.dispatch(s)
		if resp == nil {
			//println("no response")
			return
		}
		data, err := json.Marshal(resp)
		if err != nil {
			println("no json")
			return
		}
		s.process(resp, data)
		fmt.Printf("%s\n", data)
		_, err = w.Write(data)
		if err != nil {
			//println("cannot write")
			return
		}
	} else if strings.HasSuffix(r.URL.Path, "/chartdisk.js") {

		controller_send_disk_file(w, "chartdisk.json", "text/javascript")

	} else {

		s.mutex3.Lock()
		s.listeners++
		s.mutex3.Unlock()

		var comma = false
		w.Write([]byte(`p({"Success":true, "Tx":[`))

		var sent = make(map[[32]byte]struct{})
		for i := 0; i < 10; i++ {
			time.Sleep(time.Second)

			s.mutex2.Lock()
			for txid, data := range s.done {
				if _, ok := sent[txid]; ok {
					continue
				}
				sent[txid] = struct{}{}
				if comma {
					w.Write([]byte(",\n"))
				}
				_, err := w.Write([]byte(data))
				if err != nil {
					break
				}
				comma = true
			}
			s.mutex2.Unlock()
		}

		hash, height := s.m.GetTopBlockAndHeight()

		w.Write([]byte("], \"BlockHash\":\"" + hash + "\", " +
				"\"BlockHeight\": " + fmt.Sprint(height) + "});\n"))

		s.mutex3.Lock()
		s.listeners--
		s.mutex3.Unlock()
	}
}

type server struct {
	db dbInterface
	m  minerInterface

	resolver map[uint64]map[TxId]*Request
	mutex    sync.Mutex

	client *http.Client

	mutex2 sync.Mutex
	done   map[TxId]string

	mutex3    sync.Mutex
	listeners int
}

func NewServer(db dbInterface, m minerInterface) *server {
	var s server
	s.db = db
	s.m = m
	s.client = make_client(true)
	s.resolver = make(map[uint64]map[TxId]*Request)
	s.done = make(map[TxId]string)
	return &s
}

func (s *server) GoResolve(h uint64, tx TxId, req *Request) {
	s.mutex.Lock()
	if _, ok := s.resolver[h]; !ok {
		s.resolver[h] = make(map[TxId]*Request)
		go s.GoResolveBlock(h)
	}
	s.resolver[h][tx] = req
	s.mutex.Unlock()
}
func (s *server) GoResolveBlock(height uint64) {
	block, err := dlBlock(s.client, height)
	if err != nil {
		return
	}
	s.mutex.Lock()
	needtx := s.resolver[height]
	delete(s.resolver, height)
	s.mutex.Unlock()
	for _, tx := range block.Tx {
		h, err := hex.DecodeString(reversehex(tx.Txid))
		if err != nil {
			continue
		}
		var txhash [32]byte
		copy(txhash[:], h)
		s.mutex.Lock()
		req, ok := needtx[txhash]
		s.mutex.Unlock()
		if !ok {
			continue
		}
		var allOk = true
		for _, in := range req.TxIn {
			if txhash == in.PreviousOutPoint.TxId {

				amt := int64(tx.Out[in.PreviousOutPoint.Index].Value * 100000000)

				in.Amount = amt
			}
			if in.Amount == 0 {
				allOk = false
			}
		}
		if allOk {
			resp := req.dispatch2(s)
			if resp != nil {
				data, err := json.Marshal(resp)
				if err != nil {
					println("no json")
					return
				}
				s.process(resp, data)
			}
		}
	}
}

func (s *server) ServeHTTP(rw http.ResponseWriter, hr *http.Request) {
	s.mainPage(rw, hr)
}

func Serve(db dbInterface, m minerInterface) {
	err := serve(db, m)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func serve(db dbInterface, m minerInterface) error {

	bind := "127.0.0.1:12121"

	// Setup Listen
	ln, err6 := net.Listen("tcp", bind)
	if err6 != nil {
		println(err6.Error())
		return err6
	}

	h2s := &http2.Server{}

	srv := &http.Server{
		Handler:      h2c.NewHandler(NewServer(db, m), h2s),
		WriteTimeout: 60 * time.Second,
		ReadTimeout:  60 * time.Second,
	}

	err := srv.Serve(ln)
	if err != nil {
		println(err.Error())
		return err
	}
	return nil
}
