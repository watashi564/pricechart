**Comb price charter**

Use go build to build.

Sync takes long, it synces from bitcoin genesis. There is also a secondary sync pass to pull transactions that went into claims.
After that, incremental syncs are fast (just re-run the software).

In case of crash/computer shutdown/user interrupt, just re-run the software to resume.

The db is leveldb, we store unspend transactions and their block height indices. This is small.
When block 481824 is exceeded, commitments are stored (claims are counted as ordinary commitments until 601000).
When block 601000 is exceeded, claims begin to be stored.
For every claim, we store the transactions that were inputs to the claim transaction.
During the secondary pass, we revisit the input transactions, to check the BTC amount that was paid there.
We sum them, and this allows us to calculate the fees for every claim.

In the end, price.txt and out.js is written.

Note: Reorgs are unsupported, that is why the db is forever 6 blocks late.