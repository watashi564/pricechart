package main

import "github.com/syndtr/goleveldb/leveldb"

import "fmt"

type dbInterface interface {
	Exists(p txOutPoint, height uint64) uint64
	Amount(p txOutPoint) int64
}

type dbInterfaceImpl struct {
	db *leveldb.DB
}

func (db *dbInterfaceImpl) Exists(p txOutPoint, topHeight uint64) uint64 {
	for prevheight := topHeight; prevheight <= topHeight; prevheight -= 65536 {
		prevhash := hashTxAndHeight(reversehex(fmt.Sprintf("%x",p.TxId)), prevheight>>16)
		data, err := db.db.Get(prevhash[:], nil)
		if err == leveldb.ErrNotFound {
			continue
		}
		if err != nil {
			return 0
		}
		return uint64((prevheight>>16)<<16) | (uint64(data[1]) << 8) | uint64(data[2])
	}
	return 0
}

func (db *dbInterfaceImpl) Amount(p txOutPoint) int64 {
	h := hashTxAndHeight("utxo"+fmt.Sprintf("%x",p.TxId), uint64(p.Index))
	data, err := db.db.Get(h[:], nil)
	if err == leveldb.ErrNotFound {
		return 0
	}
	if err != nil {
		return 0
	}
	var amount = decodeUint(data)
	return int64(amount)

}

