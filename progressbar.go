package main

import "fmt"

type Bar struct {
	percent int64  // progress percentage
	beg     int64  // begin progress
	cur     int64  // current progress
	total   int64  // total value for progress
	rate    string // the actual progress bar to be printed
	Graph   string // the fill value for progress bar
	Unit    string
}

func (bar *Bar) getPercent() int64 {
	if bar.total == bar.beg {
		return 0
	}

	return 100 * (bar.cur - bar.beg) / (bar.total - bar.beg)
}
func (bar *Bar) Replay() {

	bar.Play(bar.beg, bar.cur, bar.total)
}
func (bar *Bar) Play(beg, cur, total int64) {
	fmt.Print("\r")
	bar.play(beg, cur, total)
}
func (bar *Bar) play(beg, cur, total int64) {
	bar.beg = beg
	bar.cur = cur
	bar.total = total
	last := bar.percent
	bar.percent = bar.getPercent()
	if bar.percent != last {
		if last+1 == bar.percent {
			if bar.percent%2 == 0 {
				bar.rate += bar.Graph
			}
		} else {
			for 2*int64(len(bar.rate)) < bar.percent {
				bar.rate += bar.Graph
			}
		}
	}
	fmt.Printf("[%-50s]%3d%% %8d/%d %s", bar.rate, bar.percent, bar.cur, bar.total, bar.Unit)
}
func (bar *Bar) Hide() {
	fmt.Print("\r\x1B[K\r")
}

func (bar *Bar) Finish() {
	fmt.Println("")
}

func (bar *Bar) Restart(unit string) {
	bar.rate = ""
	bar.Unit = unit
}